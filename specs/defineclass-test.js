var chai = require("chai"),
	assert = chai.assert,
	expect = chai.expect,
	extend = require("../lib/extend-test.js"),
	superClass = require("../lib/defineclass.js");

describe("#defineclass", function() {
	describe("#defineClass function", function() {

		it("should test if defineClass is a function", function() {

			expect(superClass.defineClass).to.be.a("function");
		});

		it("should throw error if parameters are not passed", function() {
			
			expect(function() {
				superClass.defineClass();
			}).to.throw(Error);
		});

		it("should throw error if constructor passed is not a function", function() {
			var check = "throw",
				method  = {};

			expect(function() {
				superClass.defineClass(check,method);
			}).to.throw(TypeError);
		});

		it("should throw error if method passed is not an object", function() {
			var check = function() {},
				method  = "obj";

			expect(function() {
				superClass.defineClass(check, method)
			}).to.throw(TypeError);
		});

		it("should test if statics passed is an object", function() {
			var check = function() {},
				method = {},
				statics = "message";

			expect(function() {
				superClass.defineClass(check, method, statics);
			}).to.throw(Error);
		});

		it("should expect test to pass if parameter is passed", function() {
			var value = function() {},
				method = {};

			expect(function() {
				superClass.defineClass(value, method);
			}).to.not.throw(Error);
		});

		it("should test if method is a property in constructor prototype", function() {
			var cons = function() {},
				method = {a:"boy", b:"girl"};

			superClass.defineClass(cons, method);
			expect(cons.prototype).to.have.property("a");
		});

		it("should test if defineClass returns a constructor", function() {
			var cons = function() {},
				method = {};

			var mainClass = superClass.defineClass(cons, method);

			expect(mainClass).to.be.a("function");
			expect(mainClass).to.equal(cons);
		})
	});

	describe("#subClass", function() {
		it("should test if subClass is a function", function() {

			expect(superClass.subClass).to.be.a("function");
		});

		it("should throw error if parameters are not passed", function() {
			expect(function() {
				superClass.subClass();
			}).to.throw(Error);
		});

		it("should throw error if superclass is not a function", function() {

			var cons = {};

			expect(function() {
				superClass.subClass(cons)
			}).to.throw(Error);
		});

		it("should test if constructor is a function", function() {
			var superclass = function() {},
				constructor	= function() {},
				method = {};

			expect(function() {
				superClass.subClass(superclass, constructor, method);
			}).to.not.throw(TypeError);
		});

		it("should test if method passed is an object", function() {
			var superclass = function() {}
				constructor = function() {},
				method  = "wrong";

			expect(function() {
				superClass.subClass(superclass, constructor, method)
			}).to.throw(TypeError);
		});

		it("should test if statics passed is an object", function() {
			var superclass = function(){},
				constructor= function(){},
				method		= {},
				statics		= "string";

			expect(function() {
				superClass.subClass(superclass, constructor, method, statics)
			}).to.throw(TypeError);
		});

		it("should test if subclass prototype has superclass prototype",function() {
		
			var superclass = function(){},
		        cons = function(){},
		        method = {},
		        stat = {};

            superClass.subClass(superclass,cons,method,stat);

            expect(cons.prototype).to.equal(superclass.prototype);
            expect(cons.prototype.constructor).to.equal(cons);  
		});

	})
});