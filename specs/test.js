var chai   = require("chai"),
	extend = require("../lib/extend-test.js"),
	assert = chai.assert,
	expect = chai.expect;

describe("#playSpecs", function() {
	it("should check if function extend exist", function() {

		expect(extend).to.be.a('function');
	});

	it("should check if extend has parameters", function() {

		expect(function() {
			extend()
		}).to.throw(Error);
	});

	it("should check if properties of b are passed into a", function() {
		var a = {fname: "John"},
			b = {lname: "Doe"};

		extend(a,b);

		expect(a).to.have.property("lname");
	})
})