function extend(o,p) {
	var args = arguments,
		arr,obj;

	if(!o) {
		throw new Error("receiving parameter is not provided");
	}

	if(!p) {
		throw new Error("sending parameter is not provided");
	}

	if(typeof o == "object" && typeof p == "object") {

		arr = Array.prototype.slice.call(args, 1);

		arr.forEach(function(v,i) {
			obj = arr[i];

			for(prop in obj) {
				o[prop] = obj[prop]
			}
		});
	}
}

module.exports = extend