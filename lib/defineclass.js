var extend = require("./extend-test.js");

function defineClass(constructor, methods, statics) {
	this.methods = methods || {};
	this.statics = statics || {};

	if(!constructor) {
		throw new Error("parameter not passed");
	}

	if(typeof constructor !== "function") {
		throw new TypeError("constructor is not a function");
	}

	if(typeof this.methods !== "object") {
		throw new TypeError("method is not an object");
	} 

	if(typeof this.statics !== "object") {
		throw new TypeError("statics is not an object");
	}

	extend(constructor.prototype, this.methods);
	extend(constructor, this.statics);

	return constructor;
}


function subClass(superClass, constructor, methods, statics) {
	this.methods = methods || {};
	this.statics = statics || {};

	if(!superClass) {
		throw new Error("superClass parameter is not passed");
	}

	if(typeof superClass !== "function") {
		throw new TypeError("superClass is not a function");
	}

	if(!constructor) {
		throw new Error("constructor is not provided");
	}

	if(typeof constructor !== "function") {
		throw new TypeError("constructor is not a function");
	}

	if(typeof this.methods !== "object") {
		throw new TypeError("method is not an object");
	}

	if(typeof this.statics !== "object") {
		throw new TypeError("statics is not an object");
	}

	constructor.prototype = superClass.prototype;
    constructor.prototype.constructor = constructor;

    extend(constructor.prototype,this.methods);
    extend(constructor, this.statics);

    return constructor;
}

module.exports = {
	defineClass:defineClass,
	subClass:subClass
}